#!/usr/bin/env bash



# Description : Node-red, apache and certbot (Let's encrypt) install for Ubuntu 16.04 LTS.
# SSL certificate generation with certbot.
# Node-red configuration for a sub-domain with Apache proxy and SSL.
# Startup script for node-red with a non-root user.
# Usage : ./node-red-script.sh sub-domain$1 domain.tld$2 user$2 email$4
# Usage : ./node-red-script.sh domain.tld$1 user$2 email$3


# Node-red config file (user who launch node-red at startup)
_node-red-config-file="/home/$2/.node-red/settings.js"

# System upgrade
sudo apt-get update
sudo apt-get install && sudo apt-get upgrade -y
# Installation de nodejs LTS
# Node-je install
curl -sL https://deb.nodesource.com/setup_11.x | sudo -E bash - 
sudo apt-get install -y nodejs
# Installation de build tools pour la compilation et installation via npm
# Build tools install for comiling and installing via npm
sudo apt-get install -y build-essential
# Node-red install
sudo npm install -g --unsafe-perm node-red
# Modules for node-red install
sudo npm install -g --unsafe-perm node-red-admin
# Execute node-red at startup
sudo cat >node-red.service <<EOL
[Unit]
Description=Node-RED
After=syslog.target network.target

[Service]
ExecStart=/usr/bin/node-red
Restart=on-failure
KillSignal=SIGINT

# log output to syslog as 'node-red'
SyslogIdentifier=node-red
StandardOutput=syslog

# non-root user to run as
WorkingDirectory=/home/$2/
User=$2
Group=$2

[Install]
WantedBy=multi-user.target
EOL
sudo chown root:root node-red.service
sudo mv node-red.service /etc/systemd/system/
sudo systemctl enable node-red
# Node-red config modification : allow only connections on local interface (127.0.0.1)
sed -i 's#//uiHost#uiHost#' "$_node-red-config-file"
# Node-red manual startup
sudo systemctl start node-red
# Apache install
sudo apt-get install -y apache2
# Module activation for Apache
sudo a2enmod xml2enc headers ssl proxy proxy_balancer proxy_http proxy_wstunnel
# SSL activation
sudo a2ensite default-ssl
# Vhost for sub-domain setup
sudo cat >$1.conf <<EOL
<VirtualHost *:80>
        ServerName $1
</VirtualHost>
EOL
sudo chown root:root $1.conf && sudo chmod 644 $1.conf
sudo mv $1.conf /etc/apache2/sites-available/
sudo a2ensite $1
# Apache restart
sudo systemctl restart apache2
# Certbot install
sudo add-apt-repository -y ppa:certbot/certbot
sudo apt-get update
#sudo apt-get update
sudo apt-get install -y python-certbot-apache
# Create certificates for the domain and the sud-domain
sudo certbot --agree-tos --no-eff-email -m $3 -d $1 --apache certonly
# Vhost modifications for sud-domain (SSL configuration)
sudo cat >$1.conf <<EOL
<VirtualHost *:80>
        ServerName $1
        Redirect permanent / https://$1/
</VirtualHost>

<IfModule mod_ssl.c>
        <VirtualHost *:443>
                ServerName $1
                
                SSLCertificateFile /etc/letsencrypt/live/$1/fullchain.pem
                SSLCertificateKeyFile /etc/letsencrypt/live/$1/privkey.pem
                
                SSLEngine on
                SSLProtocol all -SSLv2 -SSLv3
                SSLHonorCipherOrder on
                SSLCompression off
                SSLOptions +FakeBasicAuth +ExportCertData +StrictRequire
                SSLCipherSuite ECDHE-ECDSA-AES256-GCM-SHA384:ECDHE-RSA-AES256-GCM-SHA384:ECDHE-ECDSA-CHACHA20-POLY1305:ECDHE-RSA-CHACHA20-POLY1305:ECDHE-ECDSA-AES128-GCM-SHA256:ECDHE-RSA-AES128-GCM-SHA256:ECDHE-ECDSA-AES256-SHA384:ECDHE-RSA-AES256-SHA384:ECDHE-ECDSA-AES128-SHA256:ECDHE-RSA-AES128-SHA256
                Header always set Strict-Transport-Security "max-age=31536000; includeSubDomains"

                ProxyPreserveHost On
                ProxyPass               /comms        ws://localhost:1880/comms
                ProxyPassReverse        /comms        ws://localhost:1880/comms
                ProxyPass               /             http://127.0.0.1:1880/
                ProxyPassReverse        /             http://127.0.0.1:1880/
        </VirtualHost>
</IfModule>
EOL
sudo chown root:root $1.conf && sudo chmod 644 $1.conf
sudo mv $1.conf /etc/apache2/sites-available/
# Apache restart
sudo systemctl restart apache2